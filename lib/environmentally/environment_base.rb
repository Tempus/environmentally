require "active_support/concern"

module Environmentally
  module EnvironmentBase
    extend ActiveSupport::Concern

    class NoEnvironmentFound < StandardError; end
    class UnregisteredNamespaceException < StandardError; end
    class UnregisteredClassName < StandardError; end

    module ClassMethods
      # initializes class registry
      def init_registry
        @registry = {}
      end

      # Public: adds the class to the registry, or raises an exception
      # if incorrect params passed
      # options Hash with :as, :value and :environment keys
      def register_value(options={})
        register_as = options.fetch(:as) { raise "Need a name to register it under" }
        class_name = options.fetch(:value) { raise "Need a class_name to register it under" }
        environment = options.fetch(:environment) { raise "Need an environment key to register it under"}

        @registry[register_as] ||= {}
        @registry[register_as][environment] = class_name
      end

      def retrieve_value(registered_namespace_name)
        retrieve_value_for_environment(current_environment, registered_namespace_name)
      end

      def retrieve_value_for_environment(environment_name, registered_factory_namespace)
        env = environments.find{|e| e == environment_name}
        raise NoEnvironmentFound if env.nil?
        namespace = @registry[registered_factory_namespace]
        raise UnregisteredNamespaceException.new("No such namespace found #{registered_factory_namespace}") if namespace.nil?
        namespace.fetch(env) {
          raise UnregisteredClassName.new("No class registered for current environment")
        }
      end

      def current_environment
        raise 'Overwrite me'
      end

      private
      def environments
        raise 'Define environments'
      end
    end

    included do |base|
      base.extend(ClassMethods)
      base.init_registry
    end
  end
end
