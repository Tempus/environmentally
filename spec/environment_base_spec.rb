require 'environmentally'

describe Environmentally::EnvironmentBase do
  class EnvTested
    include Environmentally::EnvironmentBase

    def self.environments
      [:environment1, :environment2]
    end

    def self.current_environment
      :environment1
    end
  end

  before(:each) do
    EnvTested.register_value(as: :ninja, value: 'environment1 potato', environment: :environment1)
    EnvTested.register_value(as: :ninja, value: 'environment2 potato', environment: :environment2)
  end

  it 'should return proper value for environment 1' do
    EnvTested.retrieve_value(:ninja).should == "environment1 potato"
  end

  it 'should return proper value for environment 2 when environment is not the one currently running in' do
    EnvTested.retrieve_value_for_environment(:environment2, :ninja).should == "environment2 potato"
  end

  it 'should return proper value for environment 2' do
    expect(EnvTested).to receive(:current_environment).and_return(:environment2)
    EnvTested.retrieve_value(:ninja).should == "environment2 potato"
  end

  it 'should raise an EnvironmentBase::NoEnvironmentFound exception if invalid environment requested' do
    expect(EnvTested).to receive(:current_environment).and_return(:environment3)
    expect {
      EnvTested.retrieve_value(:ninja)
    }.to raise_error(Environmentally::EnvironmentBase::NoEnvironmentFound)
  end

  it 'should raise an EnvironmentBase::UnregisteredNamespaceException if invalid namespace passed' do
    expect {
      EnvTested.retrieve_value(:samurai)
    }.to raise_error(Environmentally::EnvironmentBase::UnregisteredNamespaceException)
  end

  it 'should raise an EnvironmentBase::UnregisteredNamespaceException if namespace and env are correct, but not registered for current env' do
    EnvTested.register_value(as: :samurai, value: 'samurai', environment: :environment100)
    expect {
      EnvTested.retrieve_value(:samurai)
    }.to raise_error(Environmentally::EnvironmentBase::UnregisteredClassName)
  end
end
