# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'environmentally/version'

Gem::Specification.new do |spec|
  spec.name          = "environmentally"
  spec.version       = Environmentally::VERSION
  spec.authors       = ["George Opritescu"]
  spec.email         = ["ssscripting@gmail.com"]
  spec.summary       = %q{TODO: Gem allowing for environment specific settings}
  spec.description   = %q{TODO: Gem allowing for environment specific settings}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "2.14.1"
  spec.add_dependency "activesupport", "~> 3.2.13"
end
